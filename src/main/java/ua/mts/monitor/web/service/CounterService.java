package ua.mts.monitor.web.service;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import ua.mts.monitor.web.dao.AbisRejectionCounterDao;
import ua.mts.monitor.web.dao.PacketDropRatioCounterDao;
import ua.mts.monitor.web.model.FaultyCounterInfo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by User on 11.03.2015.
 */
@Service
public class CounterService {

    @Autowired
    private AbisRejectionCounterDao abisRejectionCounterDao;

    @Autowired
    private PacketDropRatioCounterDao packetDropRatioCounterDao;

    @Value("${counter.history.hours}")
    private Integer historyHours;

    public List<FaultyCounterInfo> getFaultyCounters(String bsc) {

        List<FaultyCounterInfo> list = new ArrayList<FaultyCounterInfo>();

        Date from = DateTime.now().minusHours(historyHours).toDate();
        Date till = new Date();

        List<String> abisRejectionFaultySegments =
                abisRejectionCounterDao.getSegmentsWithFaultyCounterByBscAndDateRange(bsc,  from, till);

        List<String> packetDropRatioFaultyNseis =
                packetDropRatioCounterDao.getNseisWithFaultyCounterByBscAndDateRange(bsc, from, till);

        for (String segment : abisRejectionFaultySegments) {
            FaultyCounterInfo info = new FaultyCounterInfo();

            info.setName(String.format("%s: Segment: %s", bsc, segment));

            String url = ServletUriComponentsBuilder
                    .fromPath("api/counter/abisRejection.json")
                    .queryParam("bsc", bsc)
                    .queryParam("segment", segment)
                    .queryParam("from", from.getTime())
                    .queryParam("till", till.getTime())
                    .build()
                    .toUriString();

            info.setDataRequest(url);
            list.add(info);
        }

        for (String value : packetDropRatioFaultyNseis) {
            FaultyCounterInfo info = new FaultyCounterInfo();

            String[] values = value.split(":");
            String nsei = values[0];
            String nsvci = values[1];

            info.setName(String.format("%s: NSEI: %s NSVCI: %s", bsc, nsei, nsvci));

            String url = ServletUriComponentsBuilder
                    .fromPath("api/counter/packetDropRatio.json")
                    .queryParam("bsc", bsc)
                    .queryParam("nsei", nsei)
                    .queryParam("nsvci", nsvci)
                    .queryParam("from", from.getTime())
                    .queryParam("till", till.getTime())
                    .build()
                    .toUriString();

            info.setDataRequest(url);
            list.add(info);
        }

        return list;
    }

}
