package ua.mts.monitor.web.service;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ua.mts.monitor.web.dao.AbisRejectionCounterDao;
import ua.mts.monitor.web.dao.PacketDropRatioCounterDao;
import ua.mts.monitor.web.model.Bsc;
import ua.mts.monitor.web.model.Location;

import java.math.BigDecimal;
import java.util.*;

/**
 * Хотя тут тоже используются счетчики, сервис обслуживает уровень BSC
 */
@Service
public class BscService {

    @Autowired
    private AbisRejectionCounterDao abisRejectionCounterDao;

    @Autowired
    private PacketDropRatioCounterDao packetDropRatioCounterDao;

    @Value("${counter.history.hours}")
    private Integer historyHours;

    private List<Bsc> list;

    /**
     * Такая вот заглушка, пока нет реальных координат
     */
    public BscService() {
        list = new ArrayList<Bsc>();

        list.add(new Bsc("BSC387993", getRandomLocation()));
        list.add(new Bsc("BSC386549", getRandomLocation()));
        list.add(new Bsc("BSC387846", getRandomLocation()));
        list.add(new Bsc("BSC386559", getRandomLocation()));
        list.add(new Bsc("BSC387977", getRandomLocation()));
        list.add(new Bsc("BSC387854", getRandomLocation()));
        list.add(new Bsc("BSC395757", getRandomLocation()));
        list.add(new Bsc("BSC387995", getRandomLocation()));
        list.add(new Bsc("BSC388002", getRandomLocation()));
        list.add(new Bsc("BSC376269", getRandomLocation()));
        list.add(new Bsc("BSC386555", getRandomLocation()));
        list.add(new Bsc("BSC386562", getRandomLocation()));
        list.add(new Bsc("BSC387859", getRandomLocation()));
        list.add(new Bsc("BSC397067", getRandomLocation()));
        list.add(new Bsc("BSC376268", getRandomLocation()));
        list.add(new Bsc("BSC395113", getRandomLocation()));
        list.add(new Bsc("BSC388008", getRandomLocation()));
        list.add(new Bsc("BSC387852", getRandomLocation()));
        list.add(new Bsc("BSC387844", getRandomLocation()));
        list.add(new Bsc("BSC387851", getRandomLocation()));
        list.add(new Bsc("BSC387999", getRandomLocation()));
        list.add(new Bsc("BSC387868", getRandomLocation()));
        list.add(new Bsc("BSC395755", getRandomLocation()));
        list.add(new Bsc("BSC387991", getRandomLocation()));
        list.add(new Bsc("BSC386552", getRandomLocation()));
        list.add(new Bsc("BSC386556", getRandomLocation()));
        list.add(new Bsc("BSC386553", getRandomLocation()));
        list.add(new Bsc("BSC386557", getRandomLocation()));
        list.add(new Bsc("BSC387866", getRandomLocation()));
        list.add(new Bsc("BSC387865", getRandomLocation()));
        list.add(new Bsc("BSC386558", getRandomLocation()));
        list.add(new Bsc("BSC387988", getRandomLocation()));
        list.add(new Bsc("BSC387997", getRandomLocation()));
        list.add(new Bsc("BSC386561", getRandomLocation()));
        list.add(new Bsc("BSC388004", getRandomLocation()));
        list.add(new Bsc("BSC387979", getRandomLocation()));
        list.add(new Bsc("BSC397065", getRandomLocation()));
        list.add(new Bsc("BSC387843", getRandomLocation()));
        list.add(new Bsc("BSC387847", getRandomLocation()));
        list.add(new Bsc("BSC395756", getRandomLocation()));
        list.add(new Bsc("BSC387858", getRandomLocation()));
        list.add(new Bsc("BSC387845", getRandomLocation()));
        list.add(new Bsc("BSC387857", getRandomLocation()));
        list.add(new Bsc("BSC387855", getRandomLocation()));
        list.add(new Bsc("BSC397066", getRandomLocation()));
        list.add(new Bsc("BSC386551", getRandomLocation()));
        list.add(new Bsc("BSC387982", getRandomLocation()));
        list.add(new Bsc("BSC387848", getRandomLocation()));
        list.add(new Bsc("BSC387849", getRandomLocation()));
        list.add(new Bsc("BSC388006", getRandomLocation()));
        list.add(new Bsc("BSC387984", getRandomLocation()));
        list.add(new Bsc("BSC387853", getRandomLocation()));
        list.add(new Bsc("BSC395114", getRandomLocation()));
        list.add(new Bsc("BSC395115", getRandomLocation()));
        list.add(new Bsc("BSC387842", getRandomLocation()));
        list.add(new Bsc("BSC387856", getRandomLocation()));
        list.add(new Bsc("BSC386554", getRandomLocation()));

    }

    /**
     * Список BSC тот же, что и при создании сервиса, локации те же,
     * но статус BSC обновлен
     */
    public List<Bsc> getAllBscs() {
        Set<String> names = getFaultyBscNames();

        for (Bsc bsc : list) {
            if (names.contains(bsc.getName())) {
                bsc.setFaulty(true);
            }
        }

        return list;
    }

    /**
     * Можно было преиспользовать CounterService, но пока пусть будут
     * отдельные запросы в DAO
     */
    public Set<String> getFaultyBscNames() {
        Set<String> names = new HashSet<String>();

        Date from = DateTime.now().minusHours(historyHours).toDate();
        Date till = new Date();

        List<String> abisRejectionFaultyBscs =
                abisRejectionCounterDao.getBscsWithFaultyCounterByDateRange(from, till);

        List<String> packetDropRatioFaultyBscs =
                packetDropRatioCounterDao.getBscsWithFaultyCounterByDateRange(from, till);

        names.addAll(abisRejectionFaultyBscs);
        names.addAll(packetDropRatioFaultyBscs);

        return names;
    }

    private Location getRandomLocation() {

        Double minLat = 46d;
        Double maxLat = 51d;
        Double minLon = 25d;
        Double maxLon = 36d;

        Random r = new Random();
        double randomLat = minLat + (maxLat - minLat) * r.nextDouble();
        double randomLon = minLon + (maxLon - minLon) * r.nextDouble();

        return new Location(BigDecimal.valueOf(randomLat), BigDecimal.valueOf(randomLon));

    }

}
