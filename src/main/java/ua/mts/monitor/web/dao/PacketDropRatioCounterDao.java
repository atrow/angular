package ua.mts.monitor.web.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import ua.mts.monitor.web.model.PacketDropRatioCounter;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

/**
 * Created by User on 03.03.2015.
 */
@Repository
public class PacketDropRatioCounterDao extends AbstractDao {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public List<String> getBscsWithFaultyCounterByDateRange(Date from, Date till) {

        String query =
                "SELECT DISTINCT BSC_NUMBER \n" +
                "FROM NMC.KPI_GBIP_2_PACKET_DROP_RATIO \n" +
                "WHERE  PERIOD_REAL_START_TIME BETWEEN ? AND ? \n" +
                "ORDER BY 1";

        return jdbcTemplate.query(query, new Object[] {from, till}, getStringRowMapper());
    }

    public List<String> getNseisWithFaultyCounterByBscAndDateRange(
            String bsc, Date from, Date till) {

        String query =
                "SELECT DISTINCT (NSEI || ':' || NSVCI) \n" +
                "FROM NMC.KPI_GBIP_2_PACKET_DROP_RATIO \n" +
                "WHERE BSC_NUMBER = ? \n" +
               // "AND \"KPI_gbip_2_Packet_drop_ratio\" > 0 \n" +
                "AND PERIOD_REAL_START_TIME BETWEEN ? AND ? \n" +
                "ORDER BY 1";

        return jdbcTemplate.query(query, new Object[] {bsc, from, till}, getStringRowMapper());
    }

    public List<List<Object>> getPacketDropRatioCounters(
            String bsc, String nsei, String nsvci, Date from, Date till) {

        String query =
                "SELECT PERIOD_REAL_START_TIME, \n" +
                "\"KPI_gbip_2_Packet_drop_ratio\" AS VALUE \n" +
                "FROM NMC.KPI_GBIP_2_PACKET_DROP_RATIO \n" +
                "WHERE BSC_NUMBER = ? \n" +
                "AND NSEI = ? \n" +
                "AND NSVCI = ? \n" +
                "AND PERIOD_REAL_START_TIME BETWEEN ? AND ? \n" +
                "ORDER BY PERIOD_REAL_START_TIME";

        return jdbcTemplate.query(query,
                new Object[] {bsc, nsei, nsvci, from, till}, getDateValueListRowMapper());
    }

    private RowMapper<PacketDropRatioCounter> getPacketDropRatioCounterRowMapper() {
        return new RowMapper<PacketDropRatioCounter>(){
            @Override
            public PacketDropRatioCounter mapRow(ResultSet rs, int rowNum) throws SQLException {
                PacketDropRatioCounter counter = new PacketDropRatioCounter();
                counter.setBsc(rs.getString("BSC_NUMBER"));
                counter.setNsei(rs.getString("NSEI"));
                counter.setNsvci(rs.getString("NSVCI"));
                counter.setDate(new Date(rs.getTimestamp("PERIOD_REAL_START_TIME").getTime()));
                counter.setValue(rs.getString("VALUE"));
                return counter;
            }
        };
    }
}
