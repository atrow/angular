package ua.mts.monitor.web.dao;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by User on 11.03.2015.
 */
public class AbstractDao {

    protected RowMapper<String> getStringRowMapper() {
        return new RowMapper<String>(){
            @Override
            public String mapRow(ResultSet rs, int rowNum) throws SQLException {
                return rs.getString(1);
            }
        };
    }
    
    protected RowMapper<List<Object>> getDateValueListRowMapper() {
        return new RowMapper<List<Object>>() {
            @Override
            public List<Object> mapRow(ResultSet rs, int rowNum) throws SQLException {
                List<Object> list = new ArrayList<Object>();
                list.add(new Date(rs.getTimestamp("PERIOD_REAL_START_TIME").getTime()));
                list.add(rs.getDouble("VALUE"));
                return list;
            }
        };
    }

}
