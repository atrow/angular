package ua.mts.monitor.web.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import ua.mts.monitor.web.model.AbisRejectionCounter;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

/**
 * Created by User on 03.03.2015.
 */
@Repository
public class AbisRejectionCounterDao extends AbstractDao {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public List<String> getBscsWithFaultyCounterByDateRange(Date from, Date till) {

        String query =
                "SELECT DISTINCT BSC_NUMBER \n" +
                "FROM NMC.KPIBLCK_1000PATERRUPGREJEABIS \n" +
                "WHERE PERIOD_REAL_START_TIME BETWEEN ? AND ? \n" +
                "ORDER BY 1";

        return jdbcTemplate.query(query, new Object[] {from, till}, getStringRowMapper());
    }

    public List<String> getSegmentsWithFaultyCounterByBscAndDateRange(String bsc, Date from, Date till) {

        String query =
                "SELECT DISTINCT SEGMENT_ID \n" +
                "FROM NMC.KPIBLCK_1000PATERRUPGREJEABIS \n" +
                "WHERE BSC_NUMBER = ? \n" +
               // "AND \"KPIblck_1000PATerrUpgRejeAbis\" > 0 \n" +
                "AND PERIOD_REAL_START_TIME BETWEEN ? AND ? \n" +
                "ORDER BY 1";

        return jdbcTemplate.query(query, new Object[] {bsc, from, till}, getStringRowMapper());
    }

    public List<List<Object>> getAbisRejectionCounters(
            String bsc, String segment, Date from, Date till) {

        String query =
                "SELECT PERIOD_REAL_START_TIME, \"KPIblck_1000PATerrUpgRejeAbis\" AS VALUE\n" +
                "FROM NMC.KPIBLCK_1000PATERRUPGREJEABIS \n" +
                "WHERE BSC_NUMBER = ? \n" +
                "AND SEGMENT_ID = ? \n" +
                "AND PERIOD_REAL_START_TIME BETWEEN ? AND ? \n" +
                "ORDER BY PERIOD_REAL_START_TIME";

        return jdbcTemplate.query(query,
                new Object[] {bsc, segment, from, till}, getDateValueListRowMapper());
    }

    private RowMapper<AbisRejectionCounter> getAbisRejectionCounterRowMapper() {
        return new RowMapper<AbisRejectionCounter>(){
            @Override
            public AbisRejectionCounter mapRow(ResultSet rs, int rowNum) throws SQLException {
                AbisRejectionCounter counter = new AbisRejectionCounter();
                counter.setBsc(rs.getString("BSC_NUMBER"));
                counter.setBtsId(rs.getString("BTS_ID"));
                counter.setSegmentId(rs.getString("SEGMENT_ID"));
                counter.setDate(new Date(rs.getTimestamp("PERIOD_REAL_START_TIME").getTime()));
                counter.setValue(rs.getString("VALUE"));
                return counter;
            }
        };
    }

}
