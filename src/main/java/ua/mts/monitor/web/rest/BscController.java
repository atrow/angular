package ua.mts.monitor.web.rest;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ua.mts.monitor.web.model.Bsc;
import ua.mts.monitor.web.model.FaultyCounterInfo;
import ua.mts.monitor.web.service.BscService;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(value="/api/bsc")
public class BscController {

    @Autowired
    private BscService bscService;

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<Bsc> getAllBscs() {

        return bscService.getAllBscs();
    }

}
