package ua.mts.monitor.web.rest;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ua.mts.monitor.web.dao.AbisRejectionCounterDao;
import ua.mts.monitor.web.dao.PacketDropRatioCounterDao;
import ua.mts.monitor.web.model.AbisRejectionCounter;
import ua.mts.monitor.web.model.FaultyCounterInfo;
import ua.mts.monitor.web.model.PacketDropRatioCounter;
import ua.mts.monitor.web.service.CounterService;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(value="/api/counter")
public class CounterController {

    @Autowired
    private PacketDropRatioCounterDao packetDropRatioCounterDao;

    @Autowired
    private AbisRejectionCounterDao abisRejectionCounterDao;

    @Autowired
    private CounterService counterService;

    @RequestMapping(value = "/abisRejection", method = RequestMethod.GET)
    public List<List<Object>> getAbisRejectionCounter(
            @RequestParam("bsc") String bsc,
            @RequestParam("segment") String segment,
            @RequestParam("from") Long fromTs,
            @RequestParam("till") Long tillTs

        ) {

        Date from = new Date(fromTs);
        Date till = new Date(tillTs);

        return abisRejectionCounterDao.getAbisRejectionCounters(bsc, segment, from, till);
    }

    @RequestMapping(value = "/packetDropRatio", method = RequestMethod.GET)
    public List<List<Object>> getPacketDropRatioCounter(
            @RequestParam("bsc") String bsc,
            @RequestParam("nsei") String nsei,
            @RequestParam("nsvci") String nsvci,
            @RequestParam("from") Long fromTs,
            @RequestParam("till") Long tillTs
    ) {

        Date from = new Date(fromTs);
        Date till = new Date(tillTs);

        return packetDropRatioCounterDao.getPacketDropRatioCounters(bsc, nsei, nsvci, from, till);
    }

    @RequestMapping(value = "/faulty", method = RequestMethod.GET)
    public List<FaultyCounterInfo> getFaultyCountersInfo(
            @RequestParam("bsc") String bsc
    ) {

        return counterService.getFaultyCounters(bsc);
    }

}
