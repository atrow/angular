package ua.mts.monitor.web.model;

/**
 *
 */
public class Bsc {

    private String name;

    private Location location;

    private Boolean faulty = false;

    public Bsc() {
    }

    public Bsc(String name, Location location) {
        this.name = name;
        this.location = location;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Boolean isFaulty() {
        return faulty;
    }

    public void setFaulty(Boolean faulty) {
        this.faulty = faulty;
    }
}
