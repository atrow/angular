package ua.mts.monitor.web.model;

/**
 * Created by User on 11.03.2015.
 */
public class FaultyCounterInfo {

    private String name;

    private String dataRequest;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDataRequest() {
        return dataRequest;
    }

    public void setDataRequest(String dataRequest) {
        this.dataRequest = dataRequest;
    }
}
