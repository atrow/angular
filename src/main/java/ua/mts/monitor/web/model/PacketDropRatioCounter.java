package ua.mts.monitor.web.model;

import java.util.Date;

/**
 * Created by User on 10.03.2015.
 */
public class PacketDropRatioCounter {

    private String bsc;

    private String nsei;

    private String nsvci;

    private String value;

    private Date date;

    public String getBsc() {
        return bsc;
    }

    public void setBsc(String bsc) {
        this.bsc = bsc;
    }

    public String getNsei() {
        return nsei;
    }

    public void setNsei(String nsei) {
        this.nsei = nsei;
    }

    public String getNsvci() {
        return nsvci;
    }

    public void setNsvci(String nsvci) {
        this.nsvci = nsvci;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
