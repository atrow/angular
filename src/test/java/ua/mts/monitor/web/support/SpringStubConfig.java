package ua.mts.monitor.web.support;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

/**
 * Дополнительная конфигурация для тестов.
 * Подменяет бины, определенные в основной конфигурации, тестовыми заглушками,
 * перечисленными в ImportResource
 */
@Configuration
@ImportResource({
        "classpath:datasource-test.xml"
})
public class SpringStubConfig {
    // Empty
}
