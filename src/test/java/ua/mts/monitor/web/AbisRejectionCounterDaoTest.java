package ua.mts.monitor.web;

import org.joda.time.DateTime;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ua.mts.monitor.web.dao.AbisRejectionCounterDao;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class AbisRejectionCounterDaoTest extends AbstractSpringTest  {

    @Autowired
    AbisRejectionCounterDao abisRejectionCounterDao;

    @Test
	public void testCounters() throws Exception {

        String bsc = "BSC386549";
        String segment = "13";
        Date from = DateTime.parse("2015-02-10").toDate();
        Date till = DateTime.parse("2015-04-10").toDate();

        List<List<Object>> counters = abisRejectionCounterDao.getAbisRejectionCounters(bsc, segment, from, till);

        assertNotNull(counters);
        assertEquals(1, counters.size());
	}

    @Test
	public void testFaultySegments() throws Exception {

        String bsc = "BSC386549";
        Date from = DateTime.parse("2015-02-10").toDate();
        Date till = DateTime.parse("2015-04-10").toDate();

        List<String> segments = abisRejectionCounterDao.getSegmentsWithFaultyCounterByBscAndDateRange(bsc, from, till);

        assertNotNull(segments);
        assertEquals(8, segments.size());
	}

    @Test
    public void testFaultyBscs() throws Exception {

        Date from = DateTime.parse("2015-02-10").toDate();
        Date till = DateTime.parse("2015-04-10").toDate();

        List<String> bscs = abisRejectionCounterDao.getBscsWithFaultyCounterByDateRange(from, till);

        assertNotNull(bscs);
        assertEquals(1, bscs.size());
    }
}
