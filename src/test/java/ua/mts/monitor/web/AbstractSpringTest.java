package ua.mts.monitor.web;

import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ua.mts.monitor.web.support.SpringStubConfig;

/**
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {
        MonitorApplication.class,
        SpringStubConfig.class
})
public abstract class AbstractSpringTest {
    // Empty
}
