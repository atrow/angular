package ua.mts.monitor.web;

import org.joda.time.DateTime;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ua.mts.monitor.web.dao.PacketDropRatioCounterDao;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class PacketDropRatioCounterDaoTest extends AbstractSpringTest  {

    @Autowired
    PacketDropRatioCounterDao packetDropRatioCounterDao;

    @Test
	public void testCounters() throws Exception {

        String bsc = "BSC386549";
        Date from = DateTime.parse("2015-02-10").toDate();
        Date till = DateTime.parse("2015-04-10").toDate();

        List<List<Object>> counters = packetDropRatioCounterDao.getPacketDropRatioCounters(bsc, "58410", "1353", from, till);

        assertNotNull(counters);
        assertEquals(1, counters.size());
	}

    @Test
	public void testFaultyNseis() throws Exception {

        String bsc = "BSC386549";
        Date from = DateTime.parse("2015-02-10").toDate();
        Date till = DateTime.parse("2015-04-10").toDate();

        List<String> nseis = packetDropRatioCounterDao.getNseisWithFaultyCounterByBscAndDateRange(bsc, from, till);

        assertNotNull(nseis);
        assertEquals(8, nseis.size());
	}

    @Test
	public void testFaultyBscs() throws Exception {

        Date from = DateTime.parse("2015-02-10").toDate();
        Date till = DateTime.parse("2015-04-10").toDate();

        List<String> bscs = packetDropRatioCounterDao.getBscsWithFaultyCounterByDateRange(from, till);

        assertNotNull(bscs);
        assertEquals(46, bscs.size());
	}

}
