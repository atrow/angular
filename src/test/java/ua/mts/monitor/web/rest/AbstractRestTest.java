package ua.mts.monitor.web.rest;

import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ua.mts.monitor.web.AbstractSpringTest;

@WebAppConfiguration
public abstract class AbstractRestTest extends AbstractSpringTest {

    /**
     * Main instance of the web application context
     */
    @Autowired
    protected WebApplicationContext wac;

    /**
     * Performer of all requests
     */
    protected MockMvc mockMvc;

    @Before
    public void before() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
    }

}
