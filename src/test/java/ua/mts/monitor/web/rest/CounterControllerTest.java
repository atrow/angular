package ua.mts.monitor.web.rest;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import ua.mts.monitor.web.dao.PacketDropRatioCounterDao;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class CounterControllerTest extends AbstractRestTest {

    private static final Logger log = LoggerFactory.getLogger(CounterControllerTest.class);

    @Autowired
    private PacketDropRatioCounterDao counterDao;

    @Before
    public void before() {
        super.before();
    }

    @Test
    public void shouldGetAbisRejection() throws Exception {

        Long from = DateTime.parse("2015-02-10").toDate().getTime();
        Long till = DateTime.parse("2015-04-10").toDate().getTime();

        String response = mockMvc.perform(get("/api/counter/abisRejection")
                        .param("bsc", "BSC386549")
                        .param("segment", "12")
                        .param("from", from.toString())
                        .param("till", till.toString())
        )
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andReturn().getResponse().getContentAsString();

        log.info(response);
    }

    @Test
    public void shouldGetPacketDropRatio() throws Exception {

        Long from = DateTime.parse("2015-02-10").toDate().getTime();
        Long till = DateTime.parse("2015-04-10").toDate().getTime();

        String response = mockMvc.perform(get("/api/counter/packetDropRatio")
                        .param("bsc", "BSC386549")
                        .param("nsei", "58410")
                        .param("nsvci", "1353")
                        .param("from", from.toString())
                        .param("till", till.toString())
        )
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andReturn().getResponse().getContentAsString();

        log.info(response);
    }


    @Test
    public void shouldGetFaultyCountersInfo() throws Exception {

        String response = mockMvc.perform(get("/api/counter/faulty")
                        .param("bsc", "BSC386549")
        )
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andReturn().getResponse().getContentAsString();

        log.info(response);
    }



}
