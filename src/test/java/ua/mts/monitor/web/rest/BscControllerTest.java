package ua.mts.monitor.web.rest;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class BscControllerTest extends AbstractRestTest {

    private static final Logger log = LoggerFactory.getLogger(BscControllerTest.class);

    @Before
    public void before() {
        super.before();
    }

    @Test
    public void shouldGetAllBscs() throws Exception {

        String response = mockMvc.perform(get("/api/bsc/all")
        )
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andReturn().getResponse().getContentAsString();

        log.info(response);
    }

}
