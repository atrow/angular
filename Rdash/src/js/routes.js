'use strict';

/**
 * Route configuration for the RDash module.
 */
angular.module('RDash').config(['$stateProvider', '$urlRouterProvider',
    function($stateProvider, $urlRouterProvider) {

        // For unmatched routes
        $urlRouterProvider.otherwise('/');

        // Application routes
        $stateProvider
            .state('index', {
                url: '/',
                templateUrl: 'templates/dashboard.html',
                data: {
                    title: 'Dashboard'
                }
            })
            .state('tables', {
                url: '/sample',
                templateUrl: 'templates/sample.html',
                data: {
                    title: 'Tables'
                }
            });
    }
]);