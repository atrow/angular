/**
 * Highstock chart wrapper Directive
 */

angular
    .module('RDash')
    .directive('timelineChart', timelineChart);

function timelineChart() {
    return {
        restrict: "E",
        replace: true,
        scope: {
            params: "="
        },
        template: '<div><highchart config="chartConfig"></highchart></div>',
        controller: TimelineChartCtrl
    }
}