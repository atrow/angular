/**
 * Include on page to fire alerts from child/sibling elements
 */

angular
    .module('RDash')
    .directive('alerts', alerts);

function alerts($log) {
    return {
        restrict: "AE",
        replace: false,
        scope: {
            params: "="
        },
        templateUrl: 'templates/alerts-container.html',
        link: function (scope, element, attributes) {
            $log.log('-------Linking');
        },
        controller: AlertsCtrl
    }
}