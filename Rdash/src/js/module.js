angular.module('RDash', ['ui.bootstrap', 'ui.router', 'ngCookies', 'uiGmapgoogle-maps', 'highcharts-ng'])
    .config(function(uiGmapGoogleMapApiProvider) {
        uiGmapGoogleMapApiProvider.configure({
            v: '3.17',
            libraries: 'weather,geometry,visualization'
        });
    });