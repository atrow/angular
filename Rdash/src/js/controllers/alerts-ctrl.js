/**
 * Alerts Controller
 */

angular
    .module('RDash')
    .controller('AlertsCtrl', ['$scope', AlertsCtrl]);

function AlertsCtrl($scope, $rootScope) {
    $scope.alerts = [];

/*
    $scope.alerts = [{
        type: 'success',
        msg: 'OMG'
    }, {
        type: 'danger',
        msg: 'Found a bug? Create an issue with as many details as you can.'
    }];
*/

    $rootScope.addError = function(message) {
        $scope.alerts.push({
            type: 'danger',
            msg: message
        });
    };

    $scope.addAlert = function() {
        $scope.alerts.push({
            msg: 'Another alert!'
        });
    };

    $scope.closeAlert = function(index) {
        $scope.alerts.splice(index, 1);
    };
}