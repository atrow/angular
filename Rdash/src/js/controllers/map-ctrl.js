/**
 * Map Controller
 */

angular.module('RDash')
    .controller('MapCtrl', ['$scope', '$rootScope', '$http', '$log', 'uiGmapLogger', 'uiGmapGoogleMapApi', MapCtrl]);

function MapCtrl($scope, $rootScope, $http, $log, logger, api) {

    logger.doLog = true;
    logger.currentLevel = logger.LEVELS.debug;

    api.then(function(maps) {
        $scope.show = true;
    });

    var refresh = function () {

        $http.get("api/bsc/all.json")
            .success(function (response) {
                $log.info("Got markers count: " + response.length);
                putMarkers(response);
            })
            .error(function(data, status, headers, config) {
                $rootScope.addError(status + ' : ' + data);
            });
    };

    $scope.mapConfig = {
        center: {
            latitude: 48.6,
            longitude: 31
        },
        zoom: 6,
        options: {
            streetViewControl: false,
            panControl: false,
            maxZoom: 18,
            minZoom: 3
        },
        draggable: true,
        control: {},
        events: {}
    };

    $scope.markersConfig = {
        show: true,
        doCluster: false,
        clusterOptions: {
            minimumClusterSize: 4,
            imagePath: 'img/radio-tower',
            imageExtension: 'png',
            title: 'aaa',
            styles: [
                {height: 32, width:32, textSize: 14, textColor: 'yellow', url: 'img/radio-tower.png'},
                {height: 32, width:29, textSize: 14, textColor: 'yellow', url: 'img/radio-tower1.png'}
            ]
        }
    };

    $scope.kml = {
        options: {
            url: "http://sites.google.com/site/kmlhostinglabser/home/Ukr-Oblast-blue.kml",
            preserveViewport: true
        }
    };

    $scope.markerEvents = {
        click: function (marker, eventName, model, args, five, six) {
            $log.info('Clicked: ' + marker.key + ' : ' + model.name);
            $scope.setChartsDataUrl("api/counter/faulty.json?bsc=" + model.name);
        }
    };

    $scope.markers = [];

    var putMarkers = function (dataArray) {

        var markers = [];
        var lastId = 0;

        dataArray.forEach(function(entry) {

            markers.push({
                name: entry.name,
                location: entry.location,
                key: 'key-' + lastId++,
                icon: function() {
                    if (entry.faulty) {
                        return 'img/bsc-logo-green-alert.png';
                    } else {
                        return 'img/bsc-logo-green.png';
                    }
                }.call(),
                options: {
                    title: entry.name,
                    labelContent: entry.name,
                    labelClass: "map-label",
                    labelAnchor: "16 33"
                }
            });
        });

        $scope.markers = markers;
    };

    $scope.reset = function () {
        $scope.markers.length = 0;
    };

    refresh();
}
