/**
 * Route info Controller
 */

angular.module('RDash')
    .controller('RouteCtrl', ['$scope', '$state', RouteCtrl]);

function RouteCtrl($scope, $state) {
    /**
     * Info about current view state
     */

    $scope.getLocation = function() {
        if ($state.current.data) {
            return $state.current.data.title;
        } else {
            return null;
        }
    };

}