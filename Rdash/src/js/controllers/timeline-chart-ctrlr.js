/**
 * Highstock chart wrapper Controller
 */

angular.module('RDash')
    .controller('TimelineChartCtrl', ['$scope', '$interval', '$http', TimelineChartCtrl]);

function TimelineChartCtrl($scope, $rootScope, $interval, $http) {

    /*Задаем функцию, которая будет вызываться при изменении переменной word
     $scope.$watch(attrs.habraHabr, function(value) {
     element.text(value + attrs.habra);
     });
     */

    var refresh = function () {

        chartConfig.title.text = $scope.params.name;

        $http.get($scope.params.dataRequest)
            .success(function (response) {

                chartConfig.series = [{
                    name: $scope.params.name,
                    data: response
                }];

                chartConfig.loading = false;

            })
            .error(function(data, status, headers, config) {
                $rootScope.addError(status + ' : ' + data);
            });
    };

    var chartConfig = {

        options: {
            chart: {
                type: 'area',
                animation: false,
                borderColor: 'grey',
                borderWidth: 1
            },
            navigator: {
                enabled: true
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    animation: false,
                    step: false
                }
            },
            rangeSelector: {
                selected: 0,
                buttons: [{
                    type: 'day',
                    count: 1,
                    text: '1д'
                }, {
                    type: 'day',
                    count: 3,
                    text: '3д'
                }, {
                    type: 'day',
                    count: 7,
                    text: '7д'
                }, {
                    type: 'month',
                    count: 1,
                    text: '1мес'
                }]
            }
        },
        title: {
            text: '',
            style: {
                fontWeight: 'bold'
            }
        },
        loading: true,
        xAxis: {
            type: 'datetime'
        },
        useHighStocks: true,
        size: {
            height: 350
        }
    };

    $scope.chartConfig = chartConfig;

    refresh();
}