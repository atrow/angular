/**
 * Chart Controller
 */

angular.module('RDash')
    .controller('ChartListCtrl', ['$scope', '$interval', '$http', ChartListCtrl]);

function ChartListCtrl($scope, $interval, $http) {

    var addError = function (message) {
        $scope.$parent.addError(message);
    };

    $scope.chartParamsList = [];

    $scope.setChartsDataUrl = function (url) {
        $scope.chartsDataUrl = url;

        refresh();
    };

    var refresh = function () {

        $http.get($scope.chartsDataUrl)
            .success(function (response) {
                $scope.chartParamsList = response;
            })
            .error(function (data, status, headers, config) {
                addError(status + ' : ' + data);
            });
    };

}
